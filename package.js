Package.describe({
  name: 'core',
  version: '0.1.4',
  summary: 'Центральный пакет приложения',
  documentation: 'README.md',
  git: 'https://bitbucket.org/aspirinchaos/core.git',
});

/**
 * @module core
 */

Package.onUse(function (api) {
  api.versionsFrom('1.7');
  api.use([
    'ecmascript',
    'mongo',
    'validated-method',
    'tmeasday:check-npm-versions',
  ]);
  api.mainModule('core.js');
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('core');
  api.mainModule('core-tests.js');
});
