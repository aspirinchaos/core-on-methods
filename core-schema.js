import SimpleSchema from 'simpl-schema';
import messageBox from './message-box';

/**
 * Общая схема для наследования общих полей и переводов ошибок
 * @memberOf module:core
 * @type {SimpleSchema}
 */
const CoreSchema = new SimpleSchema({
  createdAt: {
    type: Date,
    label: 'Дата создания',
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      }
      this.unset();
    },
  },
  updatedAt: {
    type: Date,
    label: 'Дата обновления',
    autoValue: () => new Date(),
  },
});

CoreSchema.messageBox = messageBox;

export default CoreSchema;
