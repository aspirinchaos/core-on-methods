import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/validated-method';
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import CoreSchema from './core-schema';

checkNpmVersions({ 'simpl-schema': '1.5.3', 'message-box': '0.2.0' }, 'core');

/**
 * Класс для всех документов
 * @memberOf module:core
 */
class Document {
  /**
   * Класс для наследвания документов
   * @param doc {Object} - документ из монго
   */
  constructor(doc) {
    Object.assign(this, doc);
  }
}

/**
 * Класс для всех коллекций
 * @memberOf module:core
 */
class Collection {
  /**
   * Хранение схемы
   * @property {SimpleSchema}
   * @private
   */
  _schema = null;

  /**
   * Хранение класса документов
   * @property {module:core.Document}
   * @private
   */
  _docClass = null;

  /**
   * Хранение метеоровской коллекции
   * @property {Mongo.Collection}
   * @private
   */
  _mongo = null;

  /**
   * Хранение методов для работы с коллекцией
   * @type {Object}
   */
  methods = {};

  /**
   * Хранение методов для получения данных из коллекции
   * @type {Object}
   */
  finds = {};

  /**
   * Класс обертка для комфортной работы с коллекциями
   * @param {String} name - имя коллекции
   * @param {module:core.Document} DocumentClass - класс для документов коллекции
   * @param {SimpleSchema} Schema - схема для коллекции
   * @param {Object} params={} - дополнительные параметры
   * @param {Boolean} params.customFind=false - создать стандартные методы для поиска
   */
  constructor(name, DocumentClass, Schema, { customFind = false } = {}) {
    this._schema = Schema;
    this._docClass = DocumentClass;
    this._mongo = new Mongo.Collection(name, {
      // нужен для работы на сервере, т.к. данные получаем напрямую из монго
      transform(doc) {
        return new DocumentClass(doc);
      },
    });
    this._mongo.attachSchema(Schema);
    if (customFind) {
      // создадим стандартные методы для поиска
      const find = new ValidatedMethod({
        name: `${name}.find`,
        run: ({ selector = {}, options = {} } = {}) => this.mongo.find(selector, options).fetch(),
      });
      const findOne = new ValidatedMethod({
        name: `${name}.findOne`,
        run: ({ selector = {}, options = {} } = {}) => this.mongo.findOne(selector, options),
      });
      this.addFindMethod(find, 'find');
      this.addFindMethod(findOne, 'findOne', true);
    }
  }

  /**
   * Привязка метода к коллекции
   * @param {ValidatedMethod} method - Привязываемый метод
   * @param {String} name - имя для привязки
   */
  addMethod(method, name) {
    if (!name || typeof name !== 'string') {
      throw new Meteor.Error('name-is-required', 'Имя метода должно быть строкой');
    }
    if (!(method instanceof ValidatedMethod)) {
      throw new Meteor.Error('add-not-validated-method', 'Необходимо добавлять только ValidatedMethod');
    }
    if (this.methods[name]) {
      throw new Meteor.Error('add-existed-validated-method', 'ValidatedMethod с таким именем уже добавлен');
    }
    this.methods[name] = method;
  }

  /**
   * Привязка метода получения данных к коллекции
   * @param {ValidatedMethod} method - Привязываемый метод
   * @param {String} name - имя для привязки
   * @param {Boolean} [one=false] - если метод должен вернуть 1 элемент
   */
  addFindMethod(method, name, one = false) {
    this.addMethod(method, name);
    if (this.finds[name]) {
      throw new Meteor.Error('add-existed-find-method', 'Такой метод find уже добавлен');
    }
    if (one) {
      this.finds[name] = data => this.methods[name].callPromise(data)
        .then(doc => doc && new this._docClass(doc));
    } else {
      this.finds[name] = data => this.methods[name].callPromise(data)
        .then(docs => docs.map(doc => new this._docClass(doc)));
    }
  }

  /**
   * Доступ к метеоровской монго
   * @type {Mongo.Collection}
   */
  get mongo() {
    return this._mongo;
  }

  /**
   * Использование монговского insert
   * @param {...*} args - данные для insert
   * @returns {*}
   */
  insert(...args) {
    return this.mongo.insert(...args);
  }

  /**
   * Использование монговского update
   * @param {...*} args - данные для update
   * @returns {*}
   */
  update(...args) {
    return this.mongo.update(...args);
  }

  /**
   * Использование монговского upsert
   * @param {...*} args - данные для upsert
   * @returns {*}
   */
  upsert(...args) {
    return this.mongo.upsert(...args);
  }

  /**
   * Использование монговского remove
   * @param {...*} args - данные для remove
   * @returns {*}
   */
  remove(...args) {
    return this.mongo.remove(...args);
  }

  /**
   * Доступ к привязанной схеме
   * @type {SimpleSchema}
   */
  get schema() {
    return this._schema;
  }

  /**
   * Доступ к node монговской rawCollection
   * @where server
   * @type {*}
   */
  get raw() {
    return this.mongo.rawCollection();
  }

  /**
   * Очистка данных, установка дефолтных значений схемой коллекции
   * @param {Object} data - объект для очистки схемой
   * @returns {Object}
   */
  clean(data) {
    return this._schema.clean(data);
  }

  /**
   * Валидация данных схемой коллекции
   * @param {Object} data - объект для валидации схемой
   */
  validate(data) {
    this._schema.validate(this.clean(data));
  }

  /**
   * Проверяет существует ли документ
   * @where server
   * @param {String|Object} _id={} - запрос для поиска
   * @return {Boolean}
   */
  exists(_id = {}) {
    return !!this.mongo.findOne(_id, { fields: { _id: 1 } });
  }
}

export { Document, Collection, CoreSchema };
